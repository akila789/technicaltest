﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using TechnicalTest.Web.Domain;
using TechnicalTest.Web.Domain.Models;
using TechnicalTest.Web.ViewModels;
using System.Data.Entity;
using System;

namespace TechnicalTest.Web.Controllers
{
    public class HomeController : Controller
    {
       private readonly TechnicalTestDbContext context;
        public HomeController()
        {
            context = new TechnicalTestDbContext();   
        }
        // GET: Home
        //This home controller index action
        public ActionResult Index()
        {
            try
            {
                var employeeDetails = this.GetEmployeeDetails();
                return View(employeeDetails);
            }
            catch(Exception e)
            {
                throw e;
            }
        }

        public List<EmployeeViewModel> GetEmployeeDetails()
        {
            try
            {
                List<EmployeeViewModel> eViewModels = new List<EmployeeViewModel>();

                List<Employee> eList = context
                                       .Employees
                                       .Include(d => d.Department)
                                       .ToList();


                // Iterate through employee list and populate employee view model. 
                foreach (var e in eList)
                {
                    //Add employees to employee view model
                    eViewModels.Add(new EmployeeViewModel()
                    {
                        Id = e.Id,
                        FirstName = e.FirstName,
                        LastName = e.LastName,
                        JobTitle = e.JobTitle,
                        HireDate = e.HireDate,
                        DepartmentName = e.Department.DepartmentName,   // Adding department name to viewModel
                        Age = DateTime.Today.Year - e.DateOfBirth.Year  //Adding age to ViewModel
                    });
                }

                return eViewModels;
            }
            catch(Exception e)
            {
                throw e;
            }
        }

        public ActionResult Calculations()
        {
            try
            {
                //Get IT employee list count
                var empListCount = context.Employees
                    .Include(d => d.Department)
                    .Where(e => e.Department.DepartmentName == "IT")
                    .ToList().Count();

                //Get highest salary
                var highestSal = context
                                .Employees
                                .Max(e => e.AnnualSalary);

                //Get employee with highest salary
                var highestEmp = context
                                 .Employees
                                 .Single(e => e.AnnualSalary == highestSal);

                ViewBag.EmpListCount = empListCount;

                return View(highestEmp);
            }
            catch(Exception e)
            {
                throw e;
            }

        }
    }
}