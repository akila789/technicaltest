﻿using System.Collections.Generic;
using TechnicalTest.Web.ViewModels;

namespace TechnicalTest.Web.Services
{
    public interface IEmplyeeService
    {
        List<EmployeeViewModel> GetEmployees();
    }
}