﻿using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using TechnicalTest.Web.Domain.Models;

namespace TechnicalTest.Web.Domain
{
    public class TechnicalTestDbContext : DbContext
    {
        public TechnicalTestDbContext() : base("TechnicalTestConnectionString")
        {
            Database.SetInitializer<TechnicalTestDbContext>(null);
        }

        public virtual IDbSet<Department> Departments { get; set; }
        public virtual IDbSet<Employee> Employees { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
            modelBuilder.Entity<Department>()
                .HasMany(d => d.Employees)
                .WithRequired(x => x.Department)
                .HasForeignKey(e => e.DepartmentId);
        }
    }
}