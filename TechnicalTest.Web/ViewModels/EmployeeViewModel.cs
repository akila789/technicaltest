﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace TechnicalTest.Web.ViewModels
{
    public class EmployeeViewModel
    {
        public int Id { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string JobTitle { get; set; }

        public DateTime DateOfBirth { get; set; }

        public DateTime HireDate { get; set; }

        public string HomePhoneNumber { get; set; }

        public decimal AnnualSalary { get; set; }

        public int DepartmentId { get; set; }

        public string DepartmentName { get; set; }

        public int Age { get; set; }

    }
}